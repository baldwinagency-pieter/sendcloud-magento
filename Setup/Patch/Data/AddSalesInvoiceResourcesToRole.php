<?php

namespace SendCloud\SendCloud\Setup\Patch\Data;

use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddSalesInvoiceResourcesToRole implements DataPatchInterface
{
    const ROLE_NAME = 'SendCloudApi';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var ModuleListInterface
     */
    private $moduleList;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ModuleListInterface $moduleList
     */
    public function __construct(ModuleDataSetupInterface $moduleDataSetup, ModuleListInterface $moduleList)
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->moduleList = $moduleList;
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        $moduleVersion = $this->moduleList->getOne('SendCloud_SendCloud')['setup_version'];

        if (version_compare($moduleVersion, '2.0.28', '<=')) {
            $connection = $this->moduleDataSetup->getConnection();
            $aclRoleTable = $this->moduleDataSetup->getTable('authorization_role');
            $aclRuleTable = $this->moduleDataSetup->getTable('authorization_rule');

            $roleId = $connection->fetchOne(
                $connection->select()
                    ->from($aclRoleTable, ['role_id'])
                    ->where('role_name = ?', self::ROLE_NAME)
            );

            if ($roleId) {
                $connection->insertOnDuplicate($aclRuleTable, [
                    [
                        'role_id' => $roleId,
                        'resource_id' => 'Magento_Sales::sales_invoice',
                        'permission' => 'allow'
                    ]
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }
}
